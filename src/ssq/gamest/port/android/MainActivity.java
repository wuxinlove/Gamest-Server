package ssq.gamest.port.android;

import ssq.gamest.R;
import ssq.utils.HTTPUtils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
{
    private Toast            toast;

    private static final int PREFERENCE_REQUEST_CODE = 1001;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setButtonHandlers();
        boolean isRunning = App.isServiceStarted();
        setButtonText(isRunning);
        setInfoText(isRunning);
        toast = new Toast(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        boolean result = true;

        switch (item.getItemId())
        {
            case R.id.menuPreference:
            {
                startActivityForResult(new Intent(this, ServerPreferenceActivity.class), PREFERENCE_REQUEST_CODE);
                break;
            }
            default:
            {
                result = super.onOptionsItemSelected(item);
            }
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case PREFERENCE_REQUEST_CODE:
            {
                break;
            }
        }
    }

    private void setButtonHandlers()
    {
        ((Button) findViewById(R.id.btnStartStop)).setOnClickListener(btnClick);
    }

    private void setButtonText(boolean isServiceRunning)
    {
        ((Button) findViewById(R.id.btnStartStop)).setText(
                (isServiceRunning ? "停止服务器" : "开启服务器"));
    }

    private void setInfoText(boolean isServiceRunning)
    {
        TextView txtLog = (TextView) findViewById(R.id.txtLog);
        String text = "服务器未运行";

        if (isServiceRunning)
        {
            String port = String.valueOf(App.getWebServerPort());
            text = "服务器正在运行\n请在与本机同一局域网的浏览器中访问\nhttp://" + HTTPUtils.getLocalIpAddress() + ":" + port;
        }
        txtLog.setText(text);
    }

    private View.OnClickListener btnClick = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnStartStop:
                {
                    Intent intent = new Intent(MainActivity.this, HTTPService.class);

                    if (App.isServiceStarted())
                    {
                        stopService(intent);
                        App.setServiceStarted(false);
                        setButtonText(false);
                        setInfoText(false);
                    }
                    else
                    {
                        int serverPort = App.getWebServerPort();
                        if (HTTPUtils.portAvailable(serverPort))
                        {
                            startService(intent);
                            App.setServiceStarted(true);
                            setButtonText(true);
                            setInfoText(true);
                        }
                        else
                        {
                            toast = Toast.makeText(getApplicationContext(), "Port " + serverPort + " already in use!", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                    break;
                }
            }
        }
    };

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}