package ssq.gamest.port.android;

import ssq.utils.android.UtilsApp;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class App extends UtilsApp
{
    protected static final String IS_SERVICE_STARTED     = "isServiceStarted";
    protected static final String WEB_PORT               = "web_port";
    protected static final int    DEFAULT_WEB_PORT       = 8080;
    protected static final String WEBSOCKET_PORT         = "websocket_port";
    protected static final int    DEFAULT_WEBSOCKET_PORT = 44343;

    private static App            instance;

    public static App getApp()
    {
        return instance;
    }

    /**
     * Init app context which is essential when using Utils. Must be called before Custom App's onCreate.
     */
    @Override
    public void onCreate()
    {
        instance = this;
        super.onCreate();

        if (isServiceStarted())
        {
            startService(new Intent(App.this, HTTPService.class));
        }
    }

    public static boolean isServiceStarted()
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApp());
        return pref.getBoolean(IS_SERVICE_STARTED, false);
    }

    public static void setServiceStarted(boolean isStarted)
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApp());
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_SERVICE_STARTED, isStarted);
        editor.commit();
    }

    protected static int getWebServerPort()
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApp());
        int serverPort = Integer.parseInt(pref.getString(App.WEB_PORT, String.valueOf(App.DEFAULT_WEB_PORT)));
        return serverPort;
    }

    protected static int getWsServerPort()
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApp());
        int serverPort = Integer.parseInt(pref.getString(App.WEBSOCKET_PORT, String.valueOf(App.DEFAULT_WEBSOCKET_PORT)));
        return serverPort;
    }
}
