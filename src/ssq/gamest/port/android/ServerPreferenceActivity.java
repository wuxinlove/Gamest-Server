package ssq.gamest.port.android;

import ssq.gamest.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ServerPreferenceActivity extends PreferenceActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }
}
