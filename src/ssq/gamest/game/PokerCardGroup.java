package ssq.gamest.game;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.eclipse.xtext.xbase.lib.Pair;

/**
 * 几张扑克牌. 提供按点数和花色分组和比较大小的方法.
 *
 * @author s
 *
 */
public class PokerCardGroup extends LinkedList<PokerCard> implements Serializable, Comparable<PokerCardGroup>
{
    private static final long                                  serialVersionUID = -3664430198519608530L;
    public final Poker                                         game;
    /**
     * 按从大到小的顺序保存所有符合的牌型. 需要时才计算
     */
    private List<Pair<Class<? extends PokerPattern>, Integer>> matchedPatterns  = new Vector<Pair<Class<? extends PokerPattern>, Integer>>();

    public PokerCardGroup(PokerCardGroup g)
    {
        super(g);
        this.game = g.game;
    }

    public LinkedList<PokerCard>[] divideByNumber()
    {
        return divideByNumber(game.settings.withColor);
    }

    /**
     * 删除指定点数花色的张数的牌, 供删除的牌多余要删除的张数时, 按order顺序. 删完之后还是有序的, 不需要重新排序.
     *
     * @param point
     *            点数. point<=0时不考虑点数
     * @param color
     *            花色. color==null时不考虑花色
     * @param num
     *            张数
     */
    public boolean remove(int point, Color color, int num)
    {
        LinkedList<Iterator<PokerCard>> toRemove = new LinkedList<Iterator<PokerCard>>();
        try
        {
            for (Iterator<PokerCard> iterator = this.iterator(); iterator.hasNext();)
            {
                PokerCard pokerCard = iterator.next();
                if ((point <= 0 || pokerCard.point == point) && (color == null || pokerCard.color.equals(color)))
                {
                    toRemove.add(iterator);
                    num--;
                }
            }

            for (Iterator<Iterator<PokerCard>> iterator = toRemove.iterator(); iterator.hasNext();)
            {
                Iterator<PokerCard> iterator2 = iterator.next();
                iterator2.remove();
            }
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /**
     * 将这组牌按几张分组.
     *
     * @return 在ssq.gamest.game.PokerOrderSettings.withColor为false的情况下: 同样点数的有n张则在下标n-1处加入这个点数. <br/>
     *         否则, withColor为true, 同样点数和花色的有n张则在下标n-1处加入这个点数.
     *
     * @see ssq.gamest.game.PokerOrderSettings.withColor
     */
    @SuppressWarnings("unchecked")
    public LinkedList<PokerCard>[] divideByNumber(boolean withColor)
    {
        LinkedList<PokerCard>[] result = new LinkedList[size()];

        int cnt = 0;
        int lastPoint = 0;
        Color lastColor = Color.none;
        for (PokerCard card : this)
        {
            if (card.point != lastPoint || withColor && card.color != lastColor)
            {
                add(withColor, result, cnt, lastPoint, lastColor);
                cnt = 1;
            }
            else
            {
                cnt++;
            }

            lastPoint = card.point;
            lastColor = card.color;
        }

        add(withColor, result, cnt, lastPoint, lastColor);
        return result;
    }

    /**
     * 将这组牌按点数分组.
     *
     * @return n点的牌有m张, 则result[n-1]包含m
     *
     */
    @SuppressWarnings("unchecked")
    public LinkedList<Integer>[] divideByPoint()
    {
        LinkedList<Integer>[] result = new LinkedList[15];

        int cnt = 0;
        int lastPoint = 0;
        for (PokerCard card : this)
        {
            if (card.point != lastPoint)
            {
                cnt = add(result, cnt, card.point);
            }
            else
            {
                cnt++;
            }

            lastPoint = card.point;
        }

        add(result, cnt, lastPoint);
        return result;
    }

    /**
     * 将这组牌按点数和花色分组.
     *
     * @return 在ssq.gamest.game.PokerOrderSettings.withColor为false的情况下: 同样点数的有n张则在下标n-1处加入这个点数. 否则, withColor为true, 同样点数和花色的有n张则在下标n-1处加入这个点数.
     *
     * @see ssq.gamest.game.PokerOrderSettings.withColor
     */
    @SuppressWarnings("unchecked")
    public LinkedList<Integer>[] divideByPointAndColor()
    {
        final boolean withColor = true;

        LinkedList<Integer>[] result = new LinkedList[75];

        int cnt = 0;
        int lastPoint = 0;
        Color lastColor = Color.none;
        for (PokerCard card : this)
        {
            if (card.point != lastPoint || withColor && card.color != lastColor)
            {
                cnt = add(result, cnt, lastPoint, lastColor);
            }
            else
            {
                cnt++;
            }

            lastPoint = card.point;
            lastColor = card.color;
        }

        add(result, cnt, lastPoint, lastColor);
        return result;
    }

    private int add(LinkedList<Integer>[] result, int cnt, int point, Color color)
    {
        if (cnt > 0)
        {
            int index = (point - 1) * 5 + color.ordinal();

            if (result[index] == null)
            {
                result[index] = new LinkedList<Integer>();
            }
            result[index].add(cnt);
            cnt = 1;
        }
        return cnt;
    }

    private int add(LinkedList<Integer>[] result, int cnt, int point)
    {
        if (cnt > 0)
        {
            int index = point - 1;

            if (result[index] == null)
            {
                result[index] = new LinkedList<Integer>();
            }
            result[index].add(cnt);
            cnt = 1;
        }
        return cnt;
    }

    private void add(boolean withColor, LinkedList<PokerCard>[] result, int cnt, int lastPoint, Color lastColor)
    {
        if (cnt > 0)
        {
            int index = cnt - 1;

            if (result[index] == null)
            {
                result[index] = new LinkedList<PokerCard>();
            }
            result[index].add(new PokerCard(lastPoint, lastColor, this.game));
        }
    }

    public PokerCardGroup(Poker game, PokerCard... cards)
    {
        super(Arrays.asList(cards));
        this.game = game;
        Collections.sort(this);
    }

    /**
     * 等同于调用mould.match(this)
     *
     * @param mould
     * @return
     */
    public int match(PokerPattern mould)
    {
        return mould.match(this);
    }

    /**
     * 把所有可以match的全部遍历
     *
     * @return
     */
    public int matchAll()
    {
        int cnt = 0;

        Vector<Class<? extends PokerPattern>> idPattern = game.settings.idPattern;

        for (int i = 0; i < idPattern.size(); i++)
        {
            Class<? extends PokerPattern> clazz = idPattern.get(i);
            try
            {
                int result = match(clazz.newInstance());
                if (result >= 0)
                {
                    cnt++;
                    matchedPatterns.add(new Pair<Class<? extends PokerPattern>, Integer>(clazz, result));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return cnt;
    }

    /**
     * 牌组的大小是偏序关系, 若无法比较大小, 返回-2.
     */
    @Override
    public int compareTo(PokerCardGroup another)
    {
        matchAll();
        another.matchAll();

        /**
         * 都是从最大的match开始比较, 第一个满足条件的即为大小关系
         */
        for (Pair<Class<? extends PokerPattern>, Integer> pattern1 : matchedPatterns)
        {
            for (Pair<Class<? extends PokerPattern>, Integer> pattern2 : another.matchedPatterns)
            {
                int result = game.settings.getOrder(pattern1.getKey(), pattern2.getKey());
                if (result == 0)
                {
                    return pattern1.getValue() > pattern2.getValue() ? 1 : pattern1.getValue() < pattern2.getValue() ? -1 : 0;
                }
                else if (result != -2)
                {
                    return result;
                }
            }
        }
        
        return -2;
    }
}
