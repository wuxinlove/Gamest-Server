package ssq.gamest.game;

import java.util.LinkedList;
import java.util.List;

public class Deck extends LinkedList<Card>
{
    public enum Visiblity
    {
        None, Self, Team, All, Vary
    };
    
    public ParticipatedEntity belongsTo;
    public String             name;
    public Visiblity          visiblity;

    public Deck(Card... cards)
    {
        for (int i = 0; i < cards.length; i++)
        {
            add(cards[i]);
        }
    }
    
    public Deck(List<Card> cards)
    {
        super(cards);
    }
}