package ssq.gamest.game.doudizhu.patterns;

import java.util.LinkedList;

import ssq.gamest.game.PokerCard;
import ssq.gamest.game.PokerCardGroup;
import ssq.gamest.game.PokerOrderSettings;
import ssq.gamest.game.PokerPattern;

public class �����ɻ� extends PokerPattern
{
    @Override
    public int match(PokerCardGroup pokerCardGroup)
    {
        try
        {
            LinkedList<PokerCard>[] cardGroups = pokerCardGroup.divideByNumber();
            PokerOrderSettings settings = pokerCardGroup.game.settings;
            
            int cnt = 0;
            for (int lasta = -1; !cardGroups[0].isEmpty() && !cardGroups[2].isEmpty(); cnt++)
            {
                int a = getOrder(settings, cardGroups[2].removeFirst());
                if (lasta >= 0 && lasta != a - 1)
                {
                    return false;
                }

                if (!(a <= getOrder(settings, PokerCard.A)))
                {
                    return false;
                }
                lasta = a;
                
                int b = getOrder(settings, cardGroups[0].removeFirst());
            }
            
            if (!(cnt >= 2))
            {
                return false;
            }
            
            if (isEmpty(cardGroups))
            {
                return;
            }
            else
            {
                return -1;
            }
        }
        catch (Exception e)
        {
            return -1;
        }
    }
}