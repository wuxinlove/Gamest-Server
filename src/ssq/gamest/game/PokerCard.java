package ssq.gamest.game;

/**
 * 储存点数和花色. 将小王作为14, 大王作为15. 点数为0代表invalid, 点数为-1代表不可见.
 *
 * @author s
 *
 */
public class PokerCard implements Card, Comparable<PokerCard>
{
    /**
     *
     */
    private static final long serialVersionUID = -5572072418535466091L;
    public final int          point;
    public final Color        color;
    public int                order;
    public final Poker        game;
    
    public PokerCard(int p, Color c, Poker g)
    {
        point = p;
        color = c;
        game = g;
        refreshOrder();
    }
    
    @Override
    public String toString()
    {
        return (point == 14 ? "joker" : point == 15 ? "Joker" : color == Color.none ? "none" : color.toString() + point) + ":" + order;
    }

    public void refreshOrder()
    {
        if (game != null)
        {
            game.settings.refreshOrder(this);
        }
        else
        {
            order = point;
        }
    }

    @Override
    public int compareTo(PokerCard another)
    {
        return order - another.order;
    }

    static public final PokerCard A     = new PokerCard(1, Color.none, null);
    static public final PokerCard As    = new PokerCard(1, Color.spades, null);
    static public final PokerCard Ah    = new PokerCard(1, Color.hearts, null);
    static public final PokerCard Ac    = new PokerCard(1, Color.clubs, null);
    static public final PokerCard Ad    = new PokerCard(1, Color.diamonds, null);
    
    static public final PokerCard _1    = new PokerCard(1, Color.none, null);
    static public final PokerCard _1s   = new PokerCard(1, Color.spades, null);
    static public final PokerCard _1h   = new PokerCard(1, Color.hearts, null);
    static public final PokerCard _1c   = new PokerCard(1, Color.clubs, null);
    static public final PokerCard _1d   = new PokerCard(1, Color.diamonds, null);
    
    static public final PokerCard _2    = new PokerCard(2, Color.none, null);
    static public final PokerCard _2s   = new PokerCard(2, Color.spades, null);
    static public final PokerCard _2h   = new PokerCard(2, Color.hearts, null);
    static public final PokerCard _2c   = new PokerCard(2, Color.clubs, null);
    static public final PokerCard _2d   = new PokerCard(2, Color.diamonds, null);
    
    static public final PokerCard _3    = new PokerCard(3, Color.none, null);
    static public final PokerCard _3s   = new PokerCard(3, Color.spades, null);
    static public final PokerCard _3h   = new PokerCard(3, Color.hearts, null);
    static public final PokerCard _3c   = new PokerCard(3, Color.clubs, null);
    static public final PokerCard _3d   = new PokerCard(3, Color.diamonds, null);
    
    static public final PokerCard _4    = new PokerCard(4, Color.none, null);
    static public final PokerCard _4s   = new PokerCard(4, Color.spades, null);
    static public final PokerCard _4h   = new PokerCard(4, Color.hearts, null);
    static public final PokerCard _4c   = new PokerCard(4, Color.clubs, null);
    static public final PokerCard _4d   = new PokerCard(4, Color.diamonds, null);
    
    static public final PokerCard _5    = new PokerCard(5, Color.none, null);
    static public final PokerCard _5s   = new PokerCard(5, Color.spades, null);
    static public final PokerCard _5h   = new PokerCard(5, Color.hearts, null);
    static public final PokerCard _5c   = new PokerCard(5, Color.clubs, null);
    static public final PokerCard _5d   = new PokerCard(5, Color.diamonds, null);
    
    static public final PokerCard _6    = new PokerCard(6, Color.none, null);
    static public final PokerCard _6s   = new PokerCard(6, Color.spades, null);
    static public final PokerCard _6h   = new PokerCard(6, Color.hearts, null);
    static public final PokerCard _6c   = new PokerCard(6, Color.clubs, null);
    static public final PokerCard _6d   = new PokerCard(6, Color.diamonds, null);

    static public final PokerCard _7    = new PokerCard(7, Color.none, null);
    static public final PokerCard _7s   = new PokerCard(7, Color.spades, null);
    static public final PokerCard _7h   = new PokerCard(7, Color.hearts, null);
    static public final PokerCard _7c   = new PokerCard(7, Color.clubs, null);
    static public final PokerCard _7d   = new PokerCard(7, Color.diamonds, null);
    
    static public final PokerCard _8    = new PokerCard(8, Color.none, null);
    static public final PokerCard _8s   = new PokerCard(8, Color.spades, null);
    static public final PokerCard _8h   = new PokerCard(8, Color.hearts, null);
    static public final PokerCard _8c   = new PokerCard(8, Color.clubs, null);
    static public final PokerCard _8d   = new PokerCard(8, Color.diamonds, null);
    
    static public final PokerCard _9    = new PokerCard(9, Color.none, null);
    static public final PokerCard _9s   = new PokerCard(9, Color.spades, null);
    static public final PokerCard _9h   = new PokerCard(9, Color.hearts, null);
    static public final PokerCard _9c   = new PokerCard(9, Color.clubs, null);
    static public final PokerCard _9d   = new PokerCard(9, Color.diamonds, null);
    
    static public final PokerCard _10   = new PokerCard(10, Color.none, null);
    static public final PokerCard _10s  = new PokerCard(10, Color.spades, null);
    static public final PokerCard _10h  = new PokerCard(10, Color.hearts, null);
    static public final PokerCard _10c  = new PokerCard(10, Color.clubs, null);
    static public final PokerCard _10d  = new PokerCard(10, Color.diamonds, null);
    
    static public final PokerCard J     = new PokerCard(11, Color.none, null);
    static public final PokerCard Js    = new PokerCard(11, Color.spades, null);
    static public final PokerCard Jh    = new PokerCard(11, Color.hearts, null);
    static public final PokerCard Jc    = new PokerCard(11, Color.clubs, null);
    static public final PokerCard Jd    = new PokerCard(11, Color.diamonds, null);
    
    static public final PokerCard Q     = new PokerCard(12, Color.none, null);
    static public final PokerCard Qs    = new PokerCard(12, Color.spades, null);
    static public final PokerCard Qh    = new PokerCard(12, Color.hearts, null);
    static public final PokerCard Qc    = new PokerCard(12, Color.clubs, null);
    static public final PokerCard Qd    = new PokerCard(12, Color.diamonds, null);
    
    static public final PokerCard K     = new PokerCard(13, Color.none, null);
    static public final PokerCard Ks    = new PokerCard(13, Color.spades, null);
    static public final PokerCard Kh    = new PokerCard(13, Color.hearts, null);
    static public final PokerCard Kc    = new PokerCard(13, Color.clubs, null);
    static public final PokerCard Kd    = new PokerCard(13, Color.diamonds, null);
    
    static public final PokerCard joker = new PokerCard(14, Color.none, null);
    static public final PokerCard Joker = new PokerCard(15, Color.none, null);
}
