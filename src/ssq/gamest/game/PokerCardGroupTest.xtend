package ssq.gamest.game;

import net.sf.json.JSONObject
import org.junit.Test
import ssq.gamest.game.doudizhu.patterns.对子

public class PokerCardGroupTest
{
    @Test
    def void testDivide()
    {
        var poker = new Poker("掼蛋");
        
        var settings = new PokerOrderSettings(
                #[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 1, 14, 15] ,
                #[Color.diamonds, Color.clubs, Color.hearts, Color.spades, Color.none],
                true, poker);
        
//        var object = JSONObject.fromObject("[[\"`others`\", \"炸弹4\", \"炸弹5\", \"同花顺\", \"炸弹6\", \"王炸\"]]");
        
        
        settings.setPatternAndOrder(#[#["`others`", "炸弹4", "炸弹5", "同花顺", "炸弹6", "王炸"], #["对子"]])
        
        
        var tmpGroup = new PokerCardGroup(poker,
                new PokerCard(2, Color.spades, poker),
                new PokerCard(3, Color.spades, poker),
                new PokerCard(3, Color.spades, poker),
                new PokerCard(3, Color.diamonds, poker),
                new PokerCard(3, Color.hearts, poker),
                new PokerCard(14, Color.none, poker),
                new PokerCard(15, Color.none, poker),
                new PokerCard(15, Color.none, poker),
                new PokerCard(15, Color.none, poker),
                new PokerCard(2, Color.spades, poker)
                );

        var tmp1 = tmpGroup.divideByNumber();
        var tmp2 = tmpGroup.divideByPoint();
        var tmp3 = tmpGroup.divideByPointAndColor();

        print(new 对子().match(tmpGroup));
        System.out.println(new 对子().match(new PokerCardGroup(poker,
                new PokerCard(3, Color.spades, poker),
                new PokerCard(3, Color.spades, poker))));

        System.out.println(new 对子().match(new PokerCardGroup(poker,
                new PokerCard(15, Color.none, poker),
                new PokerCard(15, Color.none, poker))
                ));
    }
}
