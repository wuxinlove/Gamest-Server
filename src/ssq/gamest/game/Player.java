package ssq.gamest.game;

public abstract class Player implements ParticipatedEntity
{
    public String name;
    public Team   team;
    
    public Player(String n, Team t)
    {
        name = n;
        team = t;
    }

    public Player[] getTeamates()
    {
        return team.getPlayers();
    }

    @Override
    public Player[] getPlayers()
    {
        return new Player[] { this };
    }
}