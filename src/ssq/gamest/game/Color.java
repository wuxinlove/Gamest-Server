package ssq.gamest.game;

public enum Color
{
    spades("����"), hearts("����"), clubs("�ݻ�"), diamonds("����"), none("��");
    private Color(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }

    private String name;
}
