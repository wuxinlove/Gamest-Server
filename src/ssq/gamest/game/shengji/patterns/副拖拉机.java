package ssq.gamest.game.shengji.patterns;

import java.util.LinkedList;

import ssq.gamest.game.Color;
import ssq.gamest.game.PokerCard;
import ssq.gamest.game.PokerCardGroup;
import ssq.gamest.game.PokerPattern;

public class �������� extends PokerPattern
{
    @Override
    public boolean match(PokerCardGroup group)
    {
        PokerCardGroup pokerCardGroup = new PokerCardGroup(group);

        try
        {
            LinkedList<PokerCard>[] cardGroups = pokerCardGroup.divideByNumber();

            int cnt = 0;
            int lasta = -1;
            Color A = Color.none;

            for (; !cardGroups[0].isEmpty() && !cardGroups[2].isEmpty(); cnt++)
            {
                PokerCard card = cardGroups[2].removeFirst();
                
                int a = card.point;
                if (lasta >= 0 && lasta != a - 1)
                {
                    return false;
                }
                if (!(a <= getOrder(pokerCardGroup.game.settings, PokerCard.A)))
                {
                    return false;
                }
                lasta = a;
                
                if (A == Color.none)
                {
                    A = card.color;
                }
                else if (A != card.color)
                {
                    return false;
                }
                
                if (!(A != card.game.settings.someColors.get('A' - 'A')))
                {
                    return false;
                }

            }

            if (!(cnt >= 2))
            {
                return false;
            }

            return isEmpty(cardGroups);
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
