package ssq.gamest.game;

import java.util.LinkedList;

public abstract class PokerPattern
{
    private int weight;
    
    /**
     * 尝试将pokerCardGroup匹配到本牌型上
     *
     * @param pokerCardGroup
     * @return pokerCardGroup的权重. 若match失败, 返回-1
     */
    public abstract int match(PokerCardGroup pokerCardGroup);
    
    protected int getOrder(PokerOrderSettings settings, PokerCard card)
    {
        return settings.pointOrder.indexOf(card.point);
    }
    
    protected boolean isEmpty(LinkedList<PokerCard>[] cardGroups)
    {
        for (int i = 0; i < cardGroups.length; i++)
        {
            if (cardGroups[i] != null && !cardGroups[i].isEmpty())
                return false;
        }
        return true;
    }
}