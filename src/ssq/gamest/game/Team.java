package ssq.gamest.game;

import java.util.LinkedList;

public class Team extends LinkedList<Player> implements ParticipatedEntity
{
    @Override
    public Player[] getPlayers()
    {
        return (Player[]) this.toArray();
    }
}