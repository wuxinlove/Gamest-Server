package ssq.gamest.game;

public interface ParticipatedEntity
{
    public Player[] getPlayers();
}