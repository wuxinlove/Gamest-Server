package ssq.gamest.game;

public class Poker extends CardGame
{
    public PokerOrderSettings settings;
    public final String       name;

    public Poker(String n)
    {
        name = n;
    }

    public PokerOrderSettings getSettings()
    {
        return settings;
    }
    
    public void setSettings(PokerOrderSettings settings)
    {
        this.settings = settings;
    }
    
}