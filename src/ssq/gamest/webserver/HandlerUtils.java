package ssq.gamest.webserver;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException; //#ifdef Android
import java.nio.charset.UnsupportedCharsetException; //#ifdef Java
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.ParseException;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.util.EntityUtils;

import ssq.utils.StrUtils;

abstract public class HandlerUtils implements HttpRequestHandler
{
    protected String[] errorCode;
    
    protected void sendAccessDenied(final HttpResponse response)
    {
        response.setStatusCode(HttpStatus.SC_FORBIDDEN);
        StringEntity entity = null;
        try
        {
            entity = new StringEntity(
                    "<html><body><h1>Access denied</h1></body></html>",
                    "UTF-8");
        }
        catch (UnsupportedCharsetException e) //#ifdef Java
        catch (UnsupportedEncodingException e) //#ifdef Android
        {
            e.printStackTrace();
        }
        response.setEntity(entity);
    }
    
    protected void sendNotFound(final HttpResponse response, final String path)
    {
        response.setStatusCode(HttpStatus.SC_NOT_FOUND);
        StringEntity entity = null;
        try
        {
            entity = new StringEntity(
                    "<html><body><h1>File " + path + " not found</h1></body></html>",
                    "UTF-8");
        }
        catch (UnsupportedEncodingException e) //#ifdef Android
        catch (UnsupportedCharsetException e) //#ifdef Java
        {
            e.printStackTrace();
        }
        response.setEntity(entity);
    }
    
    protected List<String> parse(HttpRequest request, List<String> l) throws MethodNotSupportedException, ParseException, IOException
    {
        String method = "";
        try
        {
            method = request.getRequestLine().getMethod().toUpperCase(Locale.ROOT);
        }
        catch (Exception e)
        {
            throw new MethodNotSupportedException(method + " method not supported");
        }
        
        URI uri;
        try
        {
            uri = new URI(request.getRequestLine().getUri());
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
            return l;
        }
        
        String path = URLDecoder.decode(uri.getPath(), "UTF-8"), query = uri.getQuery(), fragment = uri.getFragment();
        
        if (!method.equals("GET") && !method.equals("POST"))
        {
            throw new MethodNotSupportedException(method + " method not supported");
        }
        
        if (request instanceof HttpEntityEnclosingRequest)
        {
            HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
            query = EntityUtils.toString(entity, "UTF-8");
        }
        
        if (StrUtils.noContent(query))
        {
            query = "/";
        }
        query = URLDecoder.decode(query, "UTF-8");
        
        l.add(method);
        l.add(path);
        l.add(query);
        l.add(fragment);
        return l;
    }
    
    @Override
    public abstract void handle(final HttpRequest request, final HttpResponse response, final HttpContext context) throws HttpException, IOException;
}
