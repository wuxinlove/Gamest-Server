package ssq.utils;

import android.util.Log; //#ifdef Android

public class LogUtils
{
    public static final String LOG_TAG     = "ssq";
    public static final String WARNING_TAG = "ssq warning";
    
    public static void logString(String message, String tag, boolean onlyDebug)  // TO-DO ������
    {
        if (onlyDebug && !Utilities.DEBUG_ON && !`DEBUG` || !Utilities.LOG_ON)
        {
            return;
        }
        Log.i(tag == null ? LOG_TAG : tag, message); //#ifdef Android
        System.err.println((tag == null ? LOG_TAG : tag) + " : " + message); //#ifdef Java
    }
    
    public static void logString(String message)
    {
        logString(message, null, false);
    }
    
    public static void logWarningString(String message)
    {
        logWarningString(message, null, false);
    }
    
    public static void logWarningString(String message, String tag, boolean onlyDebug)
    {
        if (onlyDebug && !Utilities.DEBUG_ON && !`DEBUG` || !Utilities.LOG_ON)
        {
            return;
        }
        Log.i(tag == null ? WARNING_TAG : tag, message); //#ifdef Android
        System.err.println((tag == null ? WARNING_TAG : tag) + " : " + message); //#ifdef Java
    }
}
