package ssq.utils.android;

import ssq.utils.Utilities;
import android.app.Application;

public class UtilsApp extends Application
{
    /**
     * Init app context which is essential when using Utils. Must be called before Custom App's onCreate.
     */
    @Override
    public void onCreate()
    {
        super.onCreate();
        Utilities.initContext(this);
    }
}
