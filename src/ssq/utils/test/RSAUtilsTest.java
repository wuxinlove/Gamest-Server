package ssq.utils.test;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.junit.Test;

import ssq.utils.MD5Utils;
import ssq.utils.RSAUtils;
import ssq.utils.StrUtils;

public class RSAUtilsTest
{
    KeyPair keypair;
    
    @Test
    public void test() throws Exception
    {
        System.out.println(StrUtils.getHex(MD5Utils.getMD5Bytes("".getBytes())));
        System.out.println((byte) 0x323130);
        System.out.println(0x323130 & 255);
        System.out.println(StrUtils.getHex(new byte[32]));
        keypair = RSAUtils.genKeyPair();
        System.out.println(StrUtils.getHex(((RSAPublicKey) keypair.getPublic()).getModulus().toByteArray()));
        System.out.println(StrUtils.getHex(((RSAPublicKey) keypair.getPublic()).getPublicExponent().toByteArray()));
        System.out.println(StrUtils.getHex(((RSAPrivateKey) keypair.getPrivate()).getPrivateExponent().toByteArray()));
        System.out.println(StrUtils.getHex(((RSAPrivateKey) keypair.getPrivate()).getModulus().toByteArray()));
    }
}
