package ssq.utils.test;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import ssq.utils.GraphViz;

public class GraphVizTest
{
    @Test
    public void test() throws IOException
    {
        GraphViz gv = new GraphViz();
        gv.addln(gv.start_graph());
        gv.addln("A -> B;");
        gv.addln("A -> C;");
        gv.addln(gv.end_graph());
        System.out.println(gv.getDotSource());

        String type = "png";
        //      String type = "plain";
        File out = new File("./tmp/out." + type);
        gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type), out);
    }

    @Test
    public void test2() throws IOException
    {
        GraphViz gv = new GraphViz();
        String s = "digraph G { A -> B; A -> C;}";
        gv.setSource(s);

        String type = "png";
        //      String type = "plain";
        File out = new File("./tmp/out2." + type);
        gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type), out);
    }
}
