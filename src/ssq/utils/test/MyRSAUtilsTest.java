package ssq.utils.test;

import java.util.Calendar;
import java.util.Scanner;

import org.junit.Test;

import ssq.utils.MyRSAUtils;
import ssq.utils.StrUtils;

public class MyRSAUtilsTest
{
    MyRSAUtils[] myRSAUtils = new MyRSAUtils[30];
    
    @Test
    public void test() throws Exception
    {
        Scanner sca = new Scanner(System.in);
        String message = sca.next();
        
        long time = Calendar.getInstance().getTimeInMillis();
        for (int i = 0; i < myRSAUtils.length; i++)
        {
            myRSAUtils[i] = new MyRSAUtils();
        }
        System.out.println(Calendar.getInstance().getTimeInMillis() - time);
        
        time = Calendar.getInstance().getTimeInMillis();
        myRSAUtils[0].addConterpart(myRSAUtils[29].publicKey.getModulus(), myRSAUtils[29].publicKey.getPublicExponent());
        for (int i = 0; i < myRSAUtils.length - 1; i++)
        {
            myRSAUtils[i].addConterpart(myRSAUtils[i + 1].publicKey.getModulus(), myRSAUtils[i + 1].publicKey.getPublicExponent());
            myRSAUtils[i + 1].addConterpart(myRSAUtils[i].publicKey.getModulus(), myRSAUtils[i].publicKey.getPublicExponent());
        }
        myRSAUtils[29].addConterpart(myRSAUtils[0].publicKey.getModulus(), myRSAUtils[0].publicKey.getPublicExponent());
        System.out.println(Calendar.getInstance().getTimeInMillis() - time);
        
        time = Calendar.getInstance().getTimeInMillis();
        try
        {
            for (int i = 0; i < myRSAUtils.length - 1; i++)
            {
                String s = myRSAUtils[i].sign(message + i, 3);
                System.out.println("" + i + "to" + (i + 1) + ":" + s);
                System.out.println(myRSAUtils[i + 1].verify(s, 2));
            }
            String s = myRSAUtils[29].sign(message + 29, 3);
            System.out.println("" + 29 + "to" + 0 + ":" + s);
            System.out.println(myRSAUtils[0].verify(s, 2));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println(Calendar.getInstance().getTimeInMillis() - time);
        
        time = Calendar.getInstance().getTimeInMillis();
        try
        {
            for (int i = 0; i < 100 - 1; i++)
            {
                String s = myRSAUtils[0].sign(message + i, 3);
                System.out.println("" + i + ":" + s);
                System.out.println(myRSAUtils[1].verify(s, 2));
                
                s = myRSAUtils[1].sign(message + i, 2);
                System.out.println("" + i + ":" + s);
                System.out.println(myRSAUtils[0].verify(s, 3));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println(Calendar.getInstance().getTimeInMillis() - time);
        
        byte[] en = myRSAUtils[0].encrypt("我就是想试试长度很大怎么办lfkjasdfljsadfl;kjasd;lfk;jasd;lkf;jdas;lkfjld;aslkfjd;slakfjda;slkf;hl;aksdjfkldsafjaioewj;asjf;laksdjf;ladksf;j;ldasfkj;dflskja;alsdfj;".getBytes("utf-8"), 3);
        System.out.println(StrUtils.getHex(en));
        
        System.out.println(new String(myRSAUtils[1].decrypt(en, 2), "utf-8"));
    }
}
