create table players (
    pid integer primary key autoincrement,
    name varchar(20) unique not null,
    pass varchar(24),
    registerTime bigint,
    gender char(1) check(gender in('M','F','N')),
    played integer,
    winned integer,
    tied integer,
    money integer,
    experience integer
);

create table undone (
    pid integer,
    gid integer,
    constraint allkey primary key(pid,gid)
);

create table games(
    gid integer primary key autoincrement,
    history varchar(128)
)