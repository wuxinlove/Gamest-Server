/**
 * Created by s on 2015/1/25 0025.
 */
xhr = {
    "request": function (callback, uri, messageToSend, method) {
        var getXMLHttpRequest = function () {
            var xhr = null;
            messageToSend = encodeURI(messageToSend||"");

            if (window.XMLHttpRequest || window.ActiveXObject) {
                if (window.ActiveXObject) {
                    try {
                        xhr = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch (e) {
                        xhr = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                }
                else {
                    xhr = new XMLHttpRequest();
                }
            }
            else {
                alert("Your browser doesn't support the XMLHTTPRequest object...");
                return null;
            }
            return xhr;
        };

        method = method || "GET";
        var xhr = getXMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                if (callback != null)
                    callback(xhr.responseText);
            }
        };

        xhr.open(method, uri+((method=="GET")?"?"+messageToSend:""), true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send(messageToSend);
    }
}
;

ws = {
    "socket" : null,

    "connect": function (port) {
        var address = "ws://"+document.location.hostname+":"+(port||44343);

        this.socket = new WebSocket(address);

        this.socket.onopen = function (e) {
            console.log(e)
        };
        this.socket.onmessage = function (e) {
            console.log(e.data)
        };
        this.socket.onerror = function (e) {
            console.log(e.data)
        };
        this.socket.onclose = function (e) {
            console.log(e)
        };
    },

    "set" : function (when, func) {
        this.socket[when] = func;
    },

    "quit": function () {
        if (this.socket) {
            this.socket.close();
            console.log("ws close");
            this.socket = null;
        }
    },

    "send": function (msg) {
        this.socket.send(msg);
    }
};