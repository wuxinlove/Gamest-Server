package ssq.gamest.game;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Test;
import ssq.gamest.game.Color;
import ssq.gamest.game.Poker;
import ssq.gamest.game.PokerCard;
import ssq.gamest.game.PokerCardGroup;
import ssq.gamest.game.PokerOrderSettings;
import ssq.gamest.game.doudizhu.patterns.对子;

@SuppressWarnings("all")
public class PokerCardGroupTest {
  @Test
  public void testDivide() {
    Poker poker = new Poker("掼蛋");
    PokerOrderSettings settings = new PokerOrderSettings(
      Collections.<Integer>unmodifiableList(CollectionLiterals.<Integer>newArrayList(Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(6), Integer.valueOf(7), Integer.valueOf(8), Integer.valueOf(9), Integer.valueOf(10), Integer.valueOf(11), Integer.valueOf(12), Integer.valueOf(13), Integer.valueOf(1), Integer.valueOf(14), Integer.valueOf(15))), 
      Collections.<Color>unmodifiableList(CollectionLiterals.<Color>newArrayList(Color.diamonds, Color.clubs, Color.hearts, Color.spades, Color.none)), 
      true, poker);
    settings.setPatternAndOrder(Collections.<List<String>>unmodifiableList(CollectionLiterals.<List<String>>newArrayList(Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("`others`", "炸弹4", "炸弹5", "同花顺", "炸弹6", "王炸")), Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("对子")))));
    PokerCard _pokerCard = new PokerCard(2, Color.spades, poker);
    PokerCard _pokerCard_1 = new PokerCard(3, Color.spades, poker);
    PokerCard _pokerCard_2 = new PokerCard(3, Color.spades, poker);
    PokerCard _pokerCard_3 = new PokerCard(3, Color.diamonds, poker);
    PokerCard _pokerCard_4 = new PokerCard(3, Color.hearts, poker);
    PokerCard _pokerCard_5 = new PokerCard(14, Color.none, poker);
    PokerCard _pokerCard_6 = new PokerCard(15, Color.none, poker);
    PokerCard _pokerCard_7 = new PokerCard(15, Color.none, poker);
    PokerCard _pokerCard_8 = new PokerCard(15, Color.none, poker);
    PokerCard _pokerCard_9 = new PokerCard(2, Color.spades, poker);
    PokerCardGroup tmpGroup = new PokerCardGroup(poker, _pokerCard, _pokerCard_1, _pokerCard_2, _pokerCard_3, _pokerCard_4, _pokerCard_5, _pokerCard_6, _pokerCard_7, _pokerCard_8, _pokerCard_9);
    LinkedList<PokerCard>[] tmp1 = tmpGroup.divideByNumber();
    LinkedList<Integer>[] tmp2 = tmpGroup.divideByPoint();
    LinkedList<Integer>[] tmp3 = tmpGroup.divideByPointAndColor();
    对子 _对子 = new 对子();
    boolean _match = _对子.match(tmpGroup);
    InputOutput.<Boolean>print(Boolean.valueOf(_match));
    对子 _对子_1 = new 对子();
    PokerCard _pokerCard_10 = new PokerCard(3, Color.spades, poker);
    PokerCard _pokerCard_11 = new PokerCard(3, Color.spades, poker);
    PokerCardGroup _pokerCardGroup = new PokerCardGroup(poker, _pokerCard_10, _pokerCard_11);
    boolean _match_1 = _对子_1.match(_pokerCardGroup);
    System.out.println(_match_1);
    对子 _对子_2 = new 对子();
    PokerCard _pokerCard_12 = new PokerCard(15, Color.none, poker);
    PokerCard _pokerCard_13 = new PokerCard(15, Color.none, poker);
    PokerCardGroup _pokerCardGroup_1 = new PokerCardGroup(poker, _pokerCard_12, _pokerCard_13);
    boolean _match_2 = _对子_2.match(_pokerCardGroup_1);
    System.out.println(_match_2);
  }
}
