import chardet
import os
import shutil
import re
import sys

def trimFileName(file):
  s = file.lower()
  s = file.replace("\\", "/")
  return s

def toReg(i):
  return re.compile(i, re.I)

def work(root, pattern, target):
  root = trimFileName(root)
  pattern = toReg(pattern)
  for dirpath, dirnames, filenames in os.walk(root):
    dirpath = trimFileName(dirpath)
    for filename in filenames:
      if pattern.match(filename):
        filename = dirpath + "/" + filename
        f = open(filename,'r')
        encoding = chardet.detect(f.read())
        confidence, encoding = encoding['confidence'], encoding['encoding']
        f.close()
        
        print filename, encoding, confidence, 
        if encoding == target or encoding == "ascii" or confidence < 0.9 or encoding == None:
          print "ignored"
          continue
        else:
          shutil.copyfile(filename, filename + ".bk")
          fin = open(filename + ".bk", "r")
          fout = open(filename, "w")
          try:
            fout.write(fin.read().decode(encoding, "ignore").encode(target))
          except:
            print "error"
          finally:
            fout.close()
          fin.close()
          print "processed"
          
if __name__ == "__main__":
  root = raw_input("root: ")
  pattern = raw_input("pattern: ")
  target = raw_input("target encoding: ")
  
  if root == "": root = "D:\\projects\\android\\Gamest\\src"
  if pattern == "": pattern = ".*\\.java"
  if target == "": target = "gbk"
  
  work(root, pattern, target)